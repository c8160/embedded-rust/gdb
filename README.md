# gdb

gdb-multiarch based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is gdb?

GDB, the GNU Project debugger, allows you to see what is going on `inside'
another program while it executes -- or what another program was doing at the
moment it crashed.

See [the project website](https://www.sourceware.org/gdb/).


## How to use this image

### Replacing a natively installed gdb

```bash
$ podman run --rm -it --pid host --network host -v "$PWD:/project:z" registry.gitlab.com/c8160/embedded-rust/gdb --help
```

This will run gdb with `--help` as argument. If you don't need any file access
you can omit the `-v` argument.


### With a containerized OpenOCD

If you want to debug embedded applications via OpenOCD, you can run GDB and
OpenOCD as containers. In this case, you can forego the `--network host`
argument in the command above since you don't have to access ports on the
hosts. You only have to ensure both containers are in the same network:

```bash
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net --name openocd openocd
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net gdb
```

In this case you must make sure that the usual

```
(gdb) target remote :3333
```

from within GDB now becomes

```
(gdb) target remote openocd:3333
```

where *openocd* **must** match the name of the OpenOCD container you specified
with the `--name` flag.


## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

